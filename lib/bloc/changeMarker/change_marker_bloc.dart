import 'package:bloc/bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'change_marer_Event.dart';
import 'change_marker_State.dart';

class ChangeMarkerBloc extends Bloc<ChangeMarkerEvent, ChangeMarkerState> {
  @override
  ChangeMarkerState get initialState => ChangeMarkerState();

  @override
  Stream<ChangeMarkerState> mapEventToState(ChangeMarkerEvent event) async* {
    if (event is ChangeLocation) {
      yield ChangeMarkerState()..value = LatLng(35.2522, -122.5654785);
    }
  }
}
