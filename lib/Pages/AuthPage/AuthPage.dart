import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';

class AuthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: Center(
        child: InkWell(
          onTap: () {
            _authorizeNow();
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(13),
              ),
              color: Color(0xff115294).withOpacity(.9),
            ),
            child: Text(
              "Tap to authenticate",
              style: TextStyle(
                color: Colors.white,
                fontSize: 17,
              ),
            ),
            padding: EdgeInsets.all(40),
          ),
        ),
      ),
    );
  }

  _appBar() {
    return AppBar(
      backgroundColor: Color(0xff07396c),
      title: Text("Authentication Page"),
      centerTitle: true,
    );
  }

  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<void> _authorizeNow() async {
    bool isAuthorized = false;
    try {
      isAuthorized = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Please authenticate to complete your transaction",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print(e);
    }
  }
}
