import 'package:CWCFlutter/Pages/AuthPage/AuthPage.dart';
import 'package:CWCFlutter/Pages/Map/map.dart';
import 'package:CWCFlutter/bloc/changePage/change_Page_Event.dart';
import 'package:CWCFlutter/bloc/changePage/change_Page_bloc.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavigationBarState createState() => _BottomNavigationBarState();
}

class _BottomNavigationBarState extends State<BottomNavBar> {
  // page name for navigation between page in bottom navigation bar
  Widget _pageName = GMap();
  // width of animation
  double _width = 0.0;
  // duration of animation
  Duration _duration = Duration(milliseconds: 500);
  // color of background bottom navigation bar
  Color _background;
  //
  ChangePageBloc _bloc = ChangePageBloc();
  @override
  Widget build(BuildContext context) {
    _background = Theme.of(context).scaffoldBackgroundColor;
    return Scaffold(
      // color of background app
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      // bottom navigation bar
      bottomNavigationBar: _bottomNavBar(),
      body: BlocBuilder(
          bloc: _bloc,
          builder: (BuildContext context, state) {
            return Stack(
              alignment: AlignmentDirectional.center,
              fit: StackFit.expand,
              children: <Widget>[
                _pageName,
                Positioned(
                  // positioned of animation container
                  bottom: -100,
                  child: AnimatedContainer(
                    width: _width,
                    height: _width,
                    curve: Curves.ease,
                    duration: _duration,
                    child: Container(
                      // incres size of animation container
                      width: _width,
                      height: _width,
                      // style of animation container
                      decoration: BoxDecoration(
                        color: Theme.of(context).buttonColor,
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
    );
  }

  // bottom navigation bar
  _bottomNavBar() {
    return CurvedNavigationBar(
      
      index: 0,
      
      // time of animation is running
      animationDuration: Duration(milliseconds: 400),
      // color of background button
      buttonBackgroundColor: Color(0xff07396c),
      // color of button
      backgroundColor: Colors.transparent,
      // size of bottom navigation bar
      height: 60.0,
      // color of bottom navigation bar
      color: Color(0xff07396c),
      // items of bottom navigation bar
      items: <Widget>[
        // fist icon in bottom navigation bar
        Icon(
          Icons.my_location,
          size: 30,
          color: Theme.of(context).buttonColor,
        ),
        // second icon in bottom navigation bar

        Icon(
          Icons.touch_app,
          size: 30,
          color: Theme.of(context).buttonColor,
        ),
        // third icon in bottom navigation bar
      ],
      // tap for navigation between page
      onTap: (index) {
        // setState for reload page
        print(_bloc.state.index);
        if (index == 0) {
          _bloc.add(ChangeToZero());
        } else {
          _bloc.add(ChangeToOne());
        }
        // select page when finished delay time
          switch (index) {
            // first icon
            case 0:
              _pageName = GMap();
              break;
            // second icon
            case 1:
              _pageName = AuthPage();
              break;
          }
      },
    );
  }
}
