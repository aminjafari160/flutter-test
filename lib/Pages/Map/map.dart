import 'dart:collection';

import 'package:CWCFlutter/bloc/changeMarker/change_marer_Event.dart';
import 'package:CWCFlutter/bloc/changeMarker/change_marker_State.dart';
import 'package:CWCFlutter/bloc/changeMarker/change_marker_bloc.dart';
import 'package:flutter/material.dart';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GMap extends StatelessWidget {
  Set<Marker> _markers = HashSet<Marker>();
  Set<Polygon> _polygons = HashSet<Polygon>();
  Set<Polyline> _polylines = HashSet<Polyline>();
  Set<Circle> _circles = HashSet<Circle>();
  bool _showMapStyle = false;

  GoogleMapController _mapController;
  BitmapDescriptor _markerIcon;

  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    _setMarkerIcon();
  }

  addNotification(
    int _id,
    subTitle,
    String txtTitle,
  ) {
    var iOS = new IOSNotificationDetails();
    var android = new AndroidNotificationDetails(
        "channelId", "channelName", "channelDescription");
    var platform = new NotificationDetails(android, iOS);

    return _flutterLocalNotificationsPlugin.show(
        _id, txtTitle, subTitle, platform);
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: _context,
      builder: (_) => new AlertDialog(
        title: Text("data"),
        content: Text("$payload"),
      ),
    );
  }

  void _setMarkerIcon() async {
    _markerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'assets/noodle_icon.png');
  }

  void _toggleMapStyle() async {
    String style = await DefaultAssetBundle.of(_context)
        .loadString('assets/map_style.json');

    if (_showMapStyle) {
      _mapController.setMapStyle(style);
    } else {
      _mapController.setMapStyle(null);
    }
  }

  ChangeMarkerBloc _bloc = ChangeMarkerBloc();
  BuildContext _context;
  @override
  Widget build(BuildContext context) {
    _context = context;
    _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

    var android = AndroidInitializationSettings('@mipmap/ic_launcher');
    var ios = IOSInitializationSettings();
    var initSetting = new InitializationSettings(android, ios);
    _flutterLocalNotificationsPlugin.initialize(initSetting,
        onSelectNotification: onSelectNotification);

    return Scaffold(
      appBar: _appBar(),
      body: Stack(
        children: <Widget>[
          BlocBuilder(
            bloc: _bloc,
            builder: (BuildContext context, ChangeMarkerState state) {
              return GoogleMap(
                onTap: (v) {
                  _bloc.add(ChangeLocation());
                  addNotification(v.latitude.toInt(), "Your location marker", "${v.latitude.toString().substring(0, 6)} , ${v.longitude.toString().substring(0, 6)}");
                  print(v);
                  _markers.clear();
                  _markers.add(
                    Marker(
                      markerId: MarkerId("1"),
                      position: v,
                      infoWindow: InfoWindow(
                        title:
                            "${v.latitude.toString().substring(0, 6)} , ${v.longitude.toString().substring(0, 6)}",
                      ),
                      icon: _markerIcon,
                    ),
                  );
                },
                // onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: LatLng(34.023576, -118.256589),
                  zoom: 12,
                ),
                markers: _markers,
                polygons: _polygons,
                polylines: _polylines,
                circles: _circles,
                myLocationEnabled: true,
                myLocationButtonEnabled: true,
              );
            },
          ),
        ],
      ),
    );
  }

  _appBar(){
    return AppBar(
      backgroundColor: Color(0xff07396c),
      centerTitle: true,
        title: Text(
          'Map page',
          
        ),
      );
  }
}
