import 'package:bloc/bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
 
 import 'change_Page_Event.dart';
 import 'change_Page_State.dart';

class ChangePageBloc extends Bloc<ChangePageEvent, ChangePageState> {
  @override
  ChangePageState get initialState => ChangePageState();

  @override
  Stream<ChangePageState> mapEventToState(ChangePageEvent event) async* {
    if (event is ChangeToZero) {
      yield ChangePageState()..index = 0;
    }else if (event is ChangeToOne) {
      
      yield ChangePageState()..index = 1;
    }
  }
}
